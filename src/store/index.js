import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        navOpen: undefined,
        loggedIn: true,
    },
    mutations: {
        setNavState( state, bool ) {
            state.navOpen = bool;
        },
        toggleNav( state ) {
            state.navOpen = !state.navOpen;
        },
        login( state ) {
            state.loggedIn = true;
        },
    }
})

export default store;
