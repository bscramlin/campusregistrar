import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from "@/views/Login";
import Home from '@/views/Home.vue';
import NewApplicants from '@/views/NewApplicants.vue';
import ArchivedApplicants from "@/views/ArchivedApplicants";
import Courses from "@/views/Courses";

Vue.use( VueRouter );

const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login,
    },
    {
        path: '/welcome',
        name: 'Welcome',
        component: Home,
    },
    {
        path: '/new-applicants',
        name: 'New Applicants',
        component: NewApplicants,
    },
    {
        path: '/archived-applicants',
        name: 'Archived Applicants',
        component: ArchivedApplicants,
    },
    {
        path: '/courses',
        name: 'Courses',
        component: Courses,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
